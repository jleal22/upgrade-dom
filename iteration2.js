/* 
2.1 Inserta dinamicamente en un html un div vacio con javascript.

2.2 Inserta dinamicamente en un html un div que contenga una p con javascript.

2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.

2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'.

2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.

2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array.
const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];

2.7 Elimina todos los nodos que tengan la clase .fn-remove-me

2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div. 
	Recuerda que no solo puedes insertar elementos con .appendChild.

2.9 Inserta p con el texto 'Voy dentro!', dentro de todos los div con la clase .fn-insert-here 
*/

// 2.1
const $$div1 = document.createElement('div');
$$div1.textContent = "2.1";
document.body.appendChild($$div1);

// 2.2
const $$div2 = document.createElement('div');
const $$p2 = document.createElement('p');
$$p2.textContent = "2.2";
$$div2.appendChild($$p2);
document.body.appendChild($$div2);

// 2.3
const $$div3 = document.createElement('div');
for(let i = 1; i<=6; i++) {
    const $$p3 = document.createElement('p');
    $$p3.textContent = "2.3 - " + i;
    $$div3.appendChild($$p3);
}
document.body.appendChild($$div3);

// 2.4
const $$p4 = document.createElement('p');
$$p4.textContent = "2.4 - Soy dinámico!";
document.body.appendChild($$p4);

// 2.5
const $$h2 = document.querySelector('h2.fn-insert-here');
$$h2.textContent = '2.5 - Wubba Lubba dub dub';

// 2.6
const $$ul = document.createElement('ul');
const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];
for (const elem of apps) {
    const $$li = document.createElement('li');
    $$li.textContent = "2.6 - " + elem;
    $$ul.appendChild($$li);
}
document.body.appendChild($$ul);

// 2.7
const $$nodes7 = document.querySelectorAll('.fn-remove-me');
$$nodes7.forEach(elem => {
    document.body.removeChild(elem);
})

// 2.8
const $$nodes8 = document.querySelectorAll('div.fn-insert-here');
const $$p8 = document.createElement('p');
$$p8.textContent = "2.8 - Voy en medio!";
document.body.insertBefore($$p8, $$nodes8[1]);

// 2.9
const $$nodes9 = document.querySelectorAll('div.fn-insert-here');
console.log($$nodes9);
$$nodes9.forEach(elem => {
    const $$p9 = document.createElement('p');
    $$p9.textContent = "2.9 - Voy dentro!";
    elem.appendChild($$p9);
})
