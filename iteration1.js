/* 
1.1 Usa querySelector para mostrar por consola el botón con la clase .showme

1.2 Usa querySelector para mostrar por consola el h1 con el id #pillado

1.3 Usa querySelector para mostrar por consola todos los p

1.4 Usa querySelector para mostrar por consola todos los elementos con la clase.pokemon

1.5 Usa querySelector para mostrar por consola todos los elementos con el atributo 
data-function="testMe".

1.6 Usa querySelector para mostrar por consola el 3 personaje con el atributo 
data-function="testMe". */

let $$btn = document.querySelector('.showme');
console.log("1.1 ->", $$btn);

$$btn = document.querySelector('#pillado');
console.log("1.2 ->", $$btn);

$$btn = document.querySelectorAll('p');
$$btn.forEach( element => {
    console.log("1.3 ->", element);
})

$$btn = document.querySelectorAll('.pokemon');
$$btn.forEach( element => {
    console.log("1.4 ->", element);
})

$$btn = document.querySelectorAll('[data-function="testMe"]');
$$btn.forEach( element => {
    console.log("1.5 ->", element);
})

console.log("1.6 ->", $$btn[2]);
